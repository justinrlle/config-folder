let g:systems = {}
let g:systems.darwin = 'Darwin'
let g:systems.current = substitute(system('uname -s'), '\n', '', '')
let g:systems.linux = 'Linux'
let g:personal = {}


nnoremap <F8> :pyf /usr/local/opt/clang-format/share/clang/clang-format.py<cr>

set number
augroup RelativeNumber
  au!

  " Automatically switch between relative numbering and absolute numbering
  au InsertEnter *.* set norelativenumber
  au InsertLeave *.* set relativenumber
augroup END
set relativenumber

let g:personal.noCursorJumpFileNames = [ 'COMMIT_EDITMSG' ]
augroup vimStartup
  au!

  " When editing a file, always jump to the last known cursor position.
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") && 
    \ index(g:personal.noCursorJumpFileNames, expand('<afile>:t')) == -1 |
    \   exe "normal! g`\"" |
    \ endif
augroup END

if !has('gui_running')

  if $TERM_PROGRAM ==# 'iTerm.app'
    let &t_SI = "\<Esc>]1337;CursorShape=1\x7"
    let &t_EI = "\<Esc>]1337;CursorShape=0\x7"
  endif
else
  let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
endif


if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
      \ | wincmd p | diffthis
endif

if has('langmap') && exists('+langremap')
  " Prevent that the langmap option applies to characters that result from a
  " mapping.  If set (default), this may break plugins (but it's backward
  " compatible).
  set nolangremap
endif

let mapleader = ','
let g:mapleader = ','

set nobackup
set nowritebackup
set swapfile
set dir=~/.config/nvim/tmp/swap_files
set undodir=~/.config/nvim/tmp/undo_files
set undofile

set virtualedit=block
set splitbelow
set splitright

set showcmd
set ruler
set inccommand=nosplit
set smartcase

set shiftwidth=2
set tabstop=2
set expandtab

set listchars=tab:╺─,trail:─,extends:>,precedes:<,nbsp:+ " ,eol:╸ might be cool
set list

set cursorline
function! ColumnDelimiter()
  if len(&colorcolumn) == 0
    let &colorcolumn = join(range(81,256),',')
  else
    let &colorcolumn = ''
  endif
endfunction

nnoremap <F1> <nop>
inoremap <F1> <nop>
vnoremap <F1> <nop>
tnoremap <F1> <nop>

nnoremap <esc><esc> :noh<return><esc>
nnoremap <leader>b :ls<return>:b<space>

nnoremap n nzz

nnoremap Y y$
nnoremap H ^
nnoremap L $
nnoremap K kJ

nnoremap p ]p
nnoremap <leader>p "+]p
vnoremap <leader>p "+]p
nnoremap <leader>yy "+yy
vnoremap <leader>y "+y
nnoremap <leader>dd "+dd
vnoremap <leader>d "+d

command W w
command Q q
command E e
command WQ wq
command Wq wq
command WA wa
command Wa wa
command QA qa
command Qa qa
command Wqa wqa
command WQa wqa
command WQA wqa

inoremap jj <Esc>
function! HardMode()
  nnoremap <left> <nop>
  nnoremap <right> <nop>
  nnoremap <up> <nop>
  nnoremap <down> <nop>
  inoremap <left> <nop>
  inoremap <right> <nop>
  inoremap <up> <nop>
  inoremap <down> <nop>
endfunction
function! EasyMode()
  nnoremap <left> <left>
  nnoremap <right> <right>
  nnoremap <up> <up>
  nnoremap <down> <down>
  inoremap <left> <left>
  inoremap <right> <right>
  inoremap <up> <up>
  inoremap <down> <down>
endfunction

" Dein config----------------------------
let &runtimepath = &runtimepath . ',' . expand('~') . '/.config/nvim/dein/repos/github.com/Shougo/dein.vim'

call dein#begin(expand('~') . '/.config/nvim/dein')

" Let dein manage dein
call dein#add('Shougo/dein.vim')

call dein#add('Shougo/unite.vim')
call dein#add('Shougo/neoyank.vim')
call dein#add('tsukkee/unite-help')
call dein#add('tsukkee/unite-tag')
call dein#add('thinca/vim-unite-history')

call dein#add('mhinz/vim-grepper')

call dein#add('justinmk/vim-sneak')
call dein#add('justinmk/vim-dirvish')
call dein#add('tpope/vim-fugitive')
call dein#add('tpope/vim-commentary')

call dein#add('morhetz/gruvbox')
call dein#add('itchyny/lightline.vim')
call dein#add('shinchu/lightline-gruvbox.vim')
call dein#add('ryanoasis/vim-devicons')

call dein#add('Shougo/deoplete.nvim')

call dein#add('zchee/deoplete-clang', { 'on_ft': ['cpp', 'c'] })
call dein#add('zchee/deoplete-jedi', { 'on_ft': ['python'] })
call dein#add('Shougo/neoinclude.vim', { 'on_ft': ['cpp', 'c'] })
call dein#add('sebastianmarkow/deoplete-rust', { 'on_ft': 'rust' })
call dein#add('Shougo/neco-vim', { 'on_ft': 'vim' })

call dein#add('ervandew/supertab')
call dein#add('tpope/vim-surround')
call dein#add('jiangmiao/auto-pairs')
call dein#add('Yggdroot/indentLine')

call dein#add('w0rp/ale')

call dein#add('octol/vim-cpp-enhanced-highlight')
call dein#add('justinmk/vim-syntax-extra')
call dein#add('rust-lang/rust.vim')

call dein#add('tpope/vim-markdown')
call dein#add('cespare/vim-toml')
call dein#add('stephpy/vim-yaml')

call dein#end()

filetype plugin indent on
syntax enable

function! CheckInstall()
  if dein#check_install()
    echom "Will update dein"
    call dein#install()
  endif
  echom "No need to update dein"
endfunction

"End dein Scripts-------------------------

"Plugins options--------------------------
let g:SuperTabDefaultCompletionType = "<c-n>"
let g:indentLine_char = '│'

" -- deoplete options --
let g:deoplete#sources#clang#libclang_path = '/usr/local/Cellar/llvm/3.9.0/lib/libclang.dylib'
let g:deoplete#sources#clang#clang_header = '/usr/local/Cellar/llvm/3.9.0/lib/clang/'
let g:deoplete#sources#clang#std = { 'c': 'c99', 'cpp': 'c++1z' }
let g:deoplete#sources#rust#racer_binary = $HOME . '/.cargo/bin/racer'
let g:deoplete#sources#rust#rust_source_path = $HOME . '.multirust/toolchains/stable-x86_64-apple-darwin/lib/rustlib/src'
let g:deoplete#sources#rust#documentation_max_height=10
call deoplete#enable()

" -- gruvbox options --
set background=dark
let g:gruvbox_italic = 1
let g:gruvbox_contrast_dark = 'medium'
set termguicolors
colorscheme gruvbox

" -- syntax options --
" cpp
let g:cpp_class_scope_highlight = 1


" -- lightline options --
if dein#is_sourced('lightline.vim')
  "  Functions
  function! LightlineReadonly()
    return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? '' : ''
  endfunction

  function! LightlineModified()
    return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
  endfunction

  function! LightlineFileformat()
    return winwidth(0) > 70 ? (&fileformat . ' ' . WebDevIconsGetFileFormatSymbol()) : ''
  endfunction

  function! LightlineFiletype()
    return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
  endfunction

  function! LightlineFileencoding()
    return winwidth(0) > 70 ? (&fenc !=# '' ? &fenc : &enc) : ''
  endfunction

  function! LightlineMode()
    return winwidth(0) > 60 ? lightline#mode() : ''
  endfunction

  function! LightlineFugitive()
    if exists('*fugitive#head')
      let branch = fugitive#head()
      return branch !=# '' ? ''.branch : ''
    endif
    return ''
  endfunction

  function! LightlineFilename()
    return ('' != LightlineReadonly() ? LightlineReadonly() . ' ' : '') .
          \ (&ft == 'vimfiler' ? vimfiler#get_status_string() :
          \  &ft == 'unite' ? unite#get_status_string() :
          \  &ft == 'vimshell' ? vimshell#get_status_string() :
          \ '' != expand('%:t') ? expand('%:t') : '[No Name]') .
          \ ('' != LightlineModified() ? ' ' . LightlineModified() : '')
  endfunction
  "  Settings
  let g:lightline = {
        \   'colorscheme': 'gruvbox',
        \   'separator': { 'left': '', 'right': '' },
        \   'subseparator': { 'left': '', 'right': '' },
        \   'component_function': {
        \       'modified': 'LightlineModified',
        \       'readonly': 'LightlineReadonly',
        \       'fugitive': 'LightlineFugitive',
        \       'filename': 'LightlineFilename',
        \       'fileformat': 'LightlineFileformat',
        \       'filetype': 'LightlineFiletype',
        \       'fileencoding': 'LightlineFileencoding',
        \       'mode': 'LightlineMode',
        \   },
        \   'active': {
        \   'left': [ [ 'mode', 'paste'], 
        \             [ 'fugitive', 'filename' ] ],
        \   'right': [ [ 'lineinfo' ],
        \              [ 'percent' ],
        \              [ 'fileformat', 'fileencoding', 'filetype' ] ]
        \   } 
        \ }

  set noshowmode

  if dein#is_sourced('ale')

    function! LightlineAleStatus()
      return winwidth(0) > 70 ? ale#statusline#Status() : ''
    endfunction

    let g:lightline.component_function.ale = 'LightlineAleStatus'
    let g:lightline.active.right[2] = [ 'ale' ] + g:lightline.active.right[2]
  endif
endif

" -- ale options --

if dein#is_sourced('ale')
  let g:incs = ' -Isrc/bignum/ -Isrc/base -Isrc/AST -Itests '

  let g:ale_cpp_gcc_options = '-Wall -Wextra -std=c++14 -pedantic -Isrc ' . g:incs . ' -Wno-pragma-oncec-outside-header'

  function! GppAdvanced(bufname)
    let l:paths = split(&path, ',')
    let l:includes = ' '
    for dir in l:paths
      if strlen(dir) == 0
        let l:includes = ' -I' . expand('#' . bufnr(a:bufname) . ':h') . l:includes
      else
        let l:includes = ' -I' . dir . l:includes
      endif
    endfor
    return 'gcc -S -x c++ -fsyntax-only' . l:includes . ' ' . g:ale_cpp_gcc_options . ' -'
  endfunction

  " call ale#linter#Define('cpp', {
  "       \   'name': 'clang++',
  "       \   'output_stream': 'stderr',
  "       \   'executable': 'g++',
  "       \   'command': 'clang -S -x c++ -fsyntax-only  -Wno-pragma-oncec-outside-header'
  "       \       . g:ale_cpp_gcc_options
  "       \       . ' -',
  "       \   'callback': 'ale#handlers#HandleGCCFormat',
  "       \})
  " let g:ale_linters = { 'cpp': ['clang++'] }
  " call ale#linter#Define('cpp', {
  "       \   'name': 'g++-better',
  "       \   'output_stream': 'stderr',
  "       \   'executable': 'g++',
  "       \   'command_callback': 'GppAdvanced',
  "       \   'callback': 'ale#handlers#HandleGCCFormat',
  "       \})

  function! AleGccCommand(bufnum)
    let l:command = 'gcc -S -x c++ -fsyntax-only'
    let l:includes = ''
    for dir in split(&path, ',')
      if strlen(dir) == 0
        let l:includes = l:includes . ' -I' . expand('#' . a:bufnum . ':h')
      else
        let l:includes = l:includes . ' -I' . dir
      endif
    endfor
    return l:command . l:includes . ' -'
  endfunction

  if g:systems.darwin ==? g:systems.darwin
    " call ale#linter#Define('cpp', {
    "       \   'name': 'g++',
    "       \   'output_stream': 'stderr',
    "       \   'executable': 'g++',
    "       \   'command_callback': 'AleGccCommand',
    "       \   'callback': 'ale#handlers#HandleGCCFormat',
    "       \})
    "let g:ale_linters = { 'cpp': 'g++' }
  else
    call ale#linter#Define('cpp', {
          \   'name': 'g++',
          \   'output_stream': 'stderr',
          \   'executable': 'g++',
          \   'command_callback': 'AleGccCommand',
          \   'callback': 'ale#handlers#HandleGCCFormat',
          \})
    let g:ale_linters = { 'cpp': 'g++' }
  endif

  let g:ale_sign_column_always = 1
  let g:ale_sign_warning = '──'
  let g:ale_sign_error = ''

  let g:ale_statusline_format = [ '%d ', '%d ', '']
endif
